/*
 * TThis program implements graph. It takes a basicGraph
 * adds two functions. Open and saveAs
 */


import java.io.FileNotFoundException;
import java.io.IOException;

/**
 *
 * @author Daniel Jackson
 */
public class Graph extends BasicGraph{
    FileOpen a = new FileOpen();
    //Same constructors.
    public Graph(int a, int b, int c){
             super(a, b, c);  
    }
    public Graph(){
        super();
    }
    //Open method intializes the FileOpen, which then 
    //uses string tokenizer to graph ints from a file.
    //Once read it sets if its directed or weighted.
    //Then populates the arrays based on previous values
    public void open( String aName) throws FileNotFoundException{
        a.openForReading(aName);
        int p1 = a.getNextInt();
        int p2;
        double p3;
       setNumberOfNodes(p1);
       initializeArrays();
        p1 = a.getNextInt();
        //Sets directed if 2 or 1 if not
        if(p1 == 1)
            setDirected(false);
        else
            setDirected(true);
        p1 = a.getNextInt();
        //Weighted if 2 or not if 1
        if(p1 == 1)
            setWeighted(false);
        else
            setWeighted(true);
         p2 = 1;
        //Populates arrays so long as no 0 is returned
        while(p1 != 0 || p2 != 0){
            p1 = a.getNextInt();
            p2 = a.getNextInt();
            //Fail safe for kicking out of populating
            if(p1 == 0 || p2 == 0){
                break;
            }
            //If weighted must grab next double
            else if(isWeighted()){
                p3 = a.getNextDouble();
                setEdge(p1,p2,p3);
            }
            //If not weighted just setEdge 
            else{
                setEdge(p1,p2);
            }
            
        }
        
    }
    //SaveAs method takes a filename and uses the same
    //printmethod as BasicGraph
 @SuppressWarnings("unused")
public void saveAs(String fileName) throws FileNotFoundException,IOException{
        String out = "";
        String p = fileName;
        System.out.print(fileName);
        try{
        a.openForWriting(fileName);
        }
        catch(Exception e){
            e.printStackTrace();
        }
        
        out = "Graph = " + fileName;
    
        a.print(out);
        out = ( "Directed = " + (directed ? true : false )  );
        
        a.print(out);
        out = ( "Weighted = " + (weighted ? true : false ) );
       
        a.print(out);
        out = ( "Number of nodes = " + nodes );
      
        a.print(out);
        out = ( "Number of edges = " + edges );
 
        a.print(out);
        if( directed ) {
            for( int i = 1; i <= nodes; i++ ) {
                for( int j = 1; j <= nodes; j++ ) {
                    if( adjMatrix[ i ] [ j ] != 0 ) {
            out =( setW( 3, "" + i, 'r') + " -->" + setW( 3, "" + j, 'r') );
                        
                        a.print(out);
                        if( weighted ){
           out = ( ", weight = " + setW( 5, "" + adjMatrix[ i ] [ j ], 'r') );
                            a.print(out);
                        }
                        else{
                            out = "";
                            a.print(out);
                            
                        }
                    }
                }
            }
        }
        else {
            for( int i = 1; i <= nodes; i++ ) {
                for( int j = i; j <= nodes; j++ ) {
                    if( adjMatrix[ i ] [ j ] != 0 ) {
             out=( setW( 3, "" + i, 'r') + " <-->" + setW( 3, "" + j, 'r') );
                        a.print(out);
                        if( weighted ){
           out = ( ", weight = " + setW( 5, "" + adjMatrix[ i ] [ j ], 'r') );
                            a.print(out);
                        }
                        else{
                           out = "";
                           a.print(out);
                    
                        }
                    }
                }
         
            }
        }
    }
}

    

