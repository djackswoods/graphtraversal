/**
 * @author Bob Broeg
 * Created: Monday, December 27, 2004 11:28:07 AM
 * Modified: December, 2006
 */
import java.util.*;
import java.io.*;

/**
 *  The purpose of this class is to provide a base on which to build
 *  a more functional Graph class.
 */



@SuppressWarnings("unused")
public abstract class BasicGraph {
    
    //Some useful constants
    public static final int DEFAULT_NODES = 10;
    public static final int MAXIMUM_NODES = 50;
    public static final int NOT_A_VERTEX = 0;
    public final int DIRECTED = 2;
    public static final int UNDIRECTED = 1;
    public static final int WEIGHTED = 2;
    public final int UNWEIGHTED = 1;
    public final int MARKED = 1;
    public static final int UNMARKED = 0;
    
    public static final double INFINITY = 0x7FFF;
    
    protected int nodes;  //number of nodes in the graph
    protected static int edges;  //number of edges in the graph
    protected boolean directed;  //is the graph directed? 
    protected boolean weighted;  //is the graph weighted?
    protected String fileName;   //records the file that stores the graph
        
    protected double [] [] adjMatrix;  //The graph is represented by an adjacency matrix
    
    protected int [] inDegree;  //records the number of incoming edges for each node
    protected int [] outDegree; //records the number of outgoing edges for each node
    protected int [] topSortNumber;  //records the topological sort position of each node
    
    protected int [] mark;  //records whether a node is marked or not
    
    /**
     *  Default constructor for BasicGraph.  This creates an empty graph.  Before anything
     *  can be done with this graph, use the setNumberOfNodes() method, and then call the
     *  initializeArrays() method.
     */
    public BasicGraph( ) {
        nodes = 0;
        edges = 0;
        directed = false;
        weighted = false;
        fileName = "";
    }
    
    /**
     * Constructor that creates a graph with the given number of
     * nodes, but with no edges. The graph is also directed
     * and weighted according to the parameters.
     * @param nodes  The number of nodes in the graph
     * @param directed  Indicates if the graph is directed (2) or undirected (1)
     * @param weighted  Indicates if the graph is weighted (2) or unweighted (1)
     */
    public BasicGraph( int nodes, int directed, int weighted ) {
        
        this.nodes = nodes;
        
        inDegree = new int[ nodes + 1 ];
        outDegree = new int[ nodes + 1 ];
        topSortNumber = new int[ nodes + 1 ];
        mark = new int[ nodes + 1 ];
        adjMatrix = new double [ nodes + 1 ] [ nodes + 1 ];
        
        for( int i = 1; i <= nodes; i++ ) {
            inDegree[ i ] = outDegree[ i ] = topSortNumber[ i ] = 0;
            for( int j = 1; j <= nodes; j++ )
                adjMatrix[ i ] [ j ] = 0;
        }
        
        this.directed = ( directed == DIRECTED ? true : false );
        this.weighted = ( weighted == WEIGHTED ? true : false );
        fileName = "";
    }
    
    
    /**
     * This method assumes that the instance variable nodes has been set with a value. 
     * It then will create the arrays.  This method is designed to be used with the
     * default constructor, but not the constructor that takes parameters.
     */
    public void initializeArrays() {
        if( nodes > 0 ) {
            inDegree = new int[ nodes + 1 ];
            outDegree = new int[ nodes + 1 ];
            topSortNumber = new int[ nodes + 1 ];
            mark = new int[ nodes + 1 ];
            adjMatrix = new double [ nodes + 1 ] [ nodes + 1 ];
        
            for( int i = 1; i <= nodes; i++ ) {
                inDegree[ i ] = outDegree[ i ] = topSortNumber[ i ] = 0;
                for( int j = 1; j <= nodes; j++ )
                    adjMatrix[ i ] [ j ] = 0;
            }
        }
    }
    
    /**
     *  Tests if 1 <= v1, v2 <= nodes
     *  @return true if both v1 and v2 are in range, false otherwise
     */
    private boolean testBounds( int v1, int v2 ) {
        boolean error = false;
        
        try {
        }
        catch( ArrayIndexOutOfBoundsException e ) { error = true; }
        
        return error;
    }
    
    
    /**
     *  This is an approximation of the setw() manipulator of C++.  It 
     *  aligns a String s in a field of width w.  If j equals 'r', then 
     *  s is right-justified in the field; if j equals 'c', then s
     *  is centered in the field; otherwise, it is left-justified
     *  in the field.
     * @param w  The length of the field
     * @param s  The string to be aligned
     * @param j  Indicates if the string is to be left, right, or center aligned.
     * @return   A string of length w with String s aligned according to char j
     */
    protected String setW( int w, String s, char j ) {
        
        
        boolean addToFront = false;
        
        if( j == 'r' )              //right-justify
            while( s.length() < w )
                s = " " + s;
        
        else if( j == 'c' )     //center-justify
            while( s.length() < w ) {
                if( addToFront )
                    s = " " + s;
                else
                    s = s + " ";
                
                addToFront = !addToFront;
            }//end while
        
        else                                    //left-justify
            while( s.length() < w )
                s = s + " ";
        
        return s;
    }
    
    /**
     *  Prints the graph to standard output.
     */
    public void print(){
        System.out.println( "\tGraph = " + fileName );
        System.out.println( "\tDirected = " + (directed ? true : false ) );
        System.out.println( "\tWeighted = " + (weighted ? true : false ) );
        System.out.println( "\tNumber of nodes = " + nodes );
        System.out.println( "\tNumber of edges = " + edges );
        
        if( directed ) {
            for( int i = 1; i <= nodes; i++ ) {
                for( int j = 1; j <= nodes; j++ ) {
                    if( adjMatrix[ i ] [ j ] != 0 ) {
                        System.out.print( setW( 3, "" + i, 'r') + " -->" + setW( 3, "" + j, 'r') );
                    
                        if( weighted )
                            System.out.println( ", weight = " + setW( 5, "" + adjMatrix[ i ] [ j ], 'r') );
                        else
                            System.out.println();
                    }
                }
            }
        }
        else {
            for( int i = 1; i <= nodes; i++ ) {
                for( int j = i; j <= nodes; j++ ) {
                    if( adjMatrix[ i ] [ j ] != 0 ) {
                        System.out.print( setW( 3, "" + i, 'r') + " <-->" + setW( 3, "" + j, 'r') );
                        
                        if( weighted )
                            System.out.println( ", weight = " + setW( 5, "" + adjMatrix[ i ] [ j ], 'r') );
                        else
                            System.out.println();
                    }
                }
            }
        }
    }
    
    /**
     *  Returns the number of nodes in the graph.
     *
     *  @return The number of nodes in the graph.
     */
    public int numberOfNodes() {   
        return nodes;
    }
    
    /**
     *   Sets the number of nodes in the graph.
     */
    public void setNumberOfNodes( int nodes ) {  
        this.nodes = nodes;
    }
    
    /**
     *   Returns the number of edges in the graph
     *
     * @return  The number of edges in the graph
     */
    public int numberOfEdges() {   
        return edges;
    }
    
    /**
     *  Finds the first edge that is incident to node or a 0 if there
     *  is no edge incident to node.
     *
     *  @param node   The vertex from which the edge emminates.
     *  @return   The first column in the adjacency matrix with a non-zero entry in row node or a zero
     */
    public int getFirstEdge( int node ) { 
        int node2 = 0;
        int index = 1;
        
        if( testBounds( node, 1 ) ) {
            System.err.println( "Error in getFirstEdge(): " + node + " out of bounds");
            return 0;
        }
        
        while( (index <= nodes) && (adjMatrix[ node ] [ index ] == 0) )
            index ++;
        
        if (index <= nodes)
            node2 = index;
        
        return node2;
    }
    
    /**
     *   Finds the next edge incident to node1 after node2 or a 0 if there
     *   are no more edges incident to node1.
     * 
     *   @param node1   Identifies a row in the adjacency matrix
     *   @param node2   Identifies a column in the adjacency matrix
     *   @return   The column number of the next non-zero entry in row node1 after column node2 or a 0
     */
    public int getNextEdge( int node1, int node2 ) {
        int node3 = 0;
        int index = node2 + 1;
        
        if( testBounds( node1, node2 ) ) {
            System.err.println( "Error in getNextEdge(): " + node1 + " or " + node2 + " out of bounds");
            return 0;
        }
        
        while( (index <= nodes) && (adjMatrix[ node1 ] [ index ] == 0) )
            index ++;
        
        if (index <= nodes)
            node3 = index;
        
        return node3;
    }
    
    /**
     *  Checks if (node1, node2) is an edge.  
     *  
     *  @param node1  The outgoing node of the edge
     *  @param node2  The incoming node of the edge
     *  @return   True if (node1, node2) is an edge, otherwise return a false.
     */
    public boolean isEdge( int node1, int node2 ) {
    
        if( testBounds( node1, node2 ) ) {
            System.err.println( "Error in isEdge(): " + node1 + " or " + node2 + " out of bounds");
            return false;
        }
        
        return ( adjMatrix[ node1 ][ node2 ] != 0 ? true : false );
    }
    
    /**
     *  Creates and adds an edge with the given weight to the graph.
     *  If the graph is undirected, the edge (node2, node1, weight) is also
     *  added to the graph.
     *  
     *  @param node1   The outgoing vertex
     *  @param node2   The incoming vertex
     *  @param weight  The weight assigned to the edge
     */
    public void setEdge( int node1, int node2, double weight ){
                
        if( testBounds( node1, node2 ) ) {
            System.err.println( "Error in setEdge(): " + node1 + " or " + node2 + " out of bounds");
            return;
        }
        
        if( adjMatrix[ node1 ] [ node2 ] != 0 )
            System.err.println( "Error in graph: " + node1 + " --> " + node2 + " is already an edge" );
        else {
            adjMatrix[ node1 ] [ node2 ] = weight;
            outDegree[ node1 ]++;
            inDegree [node2 ]++;
            if( !directed ) {
                adjMatrix[ node2 ] [ node1 ] = weight;
                outDegree[ node2 ]++;
                inDegree [ node1 ]++;
            }
            
            edges ++;
            
        }
    }
    
    /**
     *  Creates and adds an edge with no weight to the graph.
     *  If the graph is undirected, the edge (node2, node1) is 
     *  also added to the graph.
     *  
     *  @param node1   The outgoing vertex
     *  @param node2   The incoming vertex
     */
    public void setEdge( int node1, int node2 ){
                
        if( testBounds( node1, node2 ) ) {
            System.err.println( "Error in setEdge(): " + node1 + " or " + node2 + " out of bounds");
            return;
        }
        
        if( adjMatrix[ node1 ] [ node2 ] != 0 )
            System.err.println( "Error in graph: " + node1 + " --> " + node2 + " is already an edge" );
        else {
            adjMatrix[ node1 ] [ node2 ] = 1;
            outDegree[ node1 ]++;
            inDegree [node2 ]++;
            if( !directed ) {
                adjMatrix[ node2 ] [ node1 ] = 1;
                outDegree[ node2 ]++;
                inDegree [ node1 ]++;
            }
        
            edges ++;
            
        }
    
    }
        
    /**
     *  Removes the edge (node1, node2) from the graph.  If the graph is
     *  undirected, the edge (node2, node1) is also removed.
     *  
     *  @param node1   The outgoing vertex
     *  @param node2   The incoming vertex
     */
    public void deleteEdge( int node1, int node2 ){
        
        if( testBounds( node1, node2 ) ) {
            System.err.println( "Error in deleteEdge(): " + node1 + " or " + node2 + " out of bounds");
            return;
        }
        
        if( adjMatrix[ node1 ] [ node2 ] == 0 )
            System.err.println( "Error in deleteEdge(): " + node1 + " --> " + node2 + " does not exist");
        else {
            adjMatrix[ node1 ] [ node2 ] = 0;
            inDegree [ node2 ]--;
            outDegree[ node1 ]--;
            if( !directed ) {
                adjMatrix[ node2 ] [ node1 ] = 0;
                inDegree [ node1 ]--;
                outDegree[ node2 ]--;
            }
			edges--;
        }
    }

    /**
     *  Returns the weight of the edge (node1, node2) or returns 0.0 if
     *  the graph is unweighted or if (node1, node2) is not an edge.
     *  
     *  @param node1   The outgoing vertex
     *  @param node2   The incoming vertex
     *  @return        The weight of the edge or 0.0
     */
    public double getWeight( int node1, int node2 ) {
        double weight = 0.0;
        
        if( testBounds( node1, node2 ) ) {
            System.err.println( "Error in getWeight(): " + node1 + " or " + node2 + " out of bounds");
            return 0.0;
        }
        
        if( !weighted )
            System.err.println( "Error in getWeight(): Graph is not weighted" );
        else if( adjMatrix[ node1 ] [ node2 ] == 0 )
            System.err.println( "Error in getWeight(): " + node1 + " --> " + node2 + " does not exist" );
        else
            weight = adjMatrix[ node1 ] [ node2 ];
        
        return weight;
    }

    /**
     *   Marks the node with the given value.
     *   
     *   @param node    The node to be marked
     *   @param value   The value of the mark
     */
    public void setMark( int node, int value ) {
        
        if( testBounds( 1, node ) ) {
            System.err.println( "Error in setMark(): " + node + " out of bounds");
            return;
        }
        
        mark[ node ] = value;
    }
    
    /**
     *  Returns the mark of the node.
     *  
     *  @param node   The node that is checked
     *  @return       The value of the mark
     */
    public int getMark( int node ) {
        
        if( testBounds( 1, node ) ) {
            System.err.println( "Error in getMark(): " + node + " out of bounds");
            return 0;
        }
        
        return mark[ node ];
    }
    
    /**
     *   Sets value of instance variable weighted.
     *   
     *   @param weighted  True if the graph is weighted, false otherwise.
     */
    public void setWeighted( boolean weighted ) {
        this.weighted = weighted;
    }
    
    /**
     *  Returns value of instance variable weighted.
     *  
     *  @return   True if the graph is weighted, false otherwise.
     */
    public boolean isWeighted() {
        return weighted;
    }
    
    /**
     *  Sets value of instance variable directed.
     *  
     *  @param directed   True if the graph is directed, false otherwise.
     */
    public void setDirected( boolean directed ) {
        this.directed = directed;
    }
    
    /**
     *  Returns value of instance variable directed
     *  
     *  @return  True if the graph is directed, false otherwise.
     */
    public boolean isDirected() {
        return directed;
    }
    
}
