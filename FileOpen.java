/*
 * This program handles tokenizing, and opening / closing 
 * file writing. 
 */

import java.io.*;
/**
 *
 * @author Daniel Jackson
 */
public class FileOpen {
    String fileName;
    StreamTokenizer token;
    InputStream s;
    PrintWriter write;
    File aFile;
    public FileOpen(){
        
    }
    //Opens file for writing, intializes tokenizer
    //Also sets for comments,
    public void openForReading(String k) throws FileNotFoundException{
        token = new StreamTokenizer(new FileReader(k));
        token.slashSlashComments(true);
    }
    //Opens saveAs file for writing
 public void openForWriting(String p)throws FileNotFoundException,IOException{
        System.out.println(p);
        aFile = new File(p); 
        write = new PrintWriter(p);
        
    }
    //Method advances the tokenizer until a int is found
    //once found it returns its. if a 0 is returned
    //that means EOF reached
    @SuppressWarnings("static-access")
	public int getNextInt(){
          String a = "a";
          int k = 0;
          while("a".equals(a)){
               try{
           token.nextToken();
               
                if(token.ttype== token.TT_NUMBER){
                   k = (int)token.nval;
                   a = "b";
                }
                if(token.ttype == token.TT_EOF){
                
                    k = 0;
                    break;
                }
                
            }
               catch(IOException e){
            
        }
        
        }
      return k;
      }
       //This method advances the tokenizer until a double is found
      // Once a double is foiund it is returned. a 0 is returned
    //for EoF
      @SuppressWarnings("static-access")
	public double getNextDouble(){
          String a = "a";
          double k = 0;
          while("a".equals(a)){
               try{
           token.nextToken();
                
                if(token.ttype== token.TT_NUMBER){
                   k = (double)token.nval;
                   return k;
                }
                if(token.ttype == token.TT_EOF){
                    break;
                }
                
            }
               catch(IOException e){
                  System.out.println("Managed to go past");
        }
        
        }
      return k;
      }
      
      //Closes print writer
      public void closeWriting(){
          write.close();
      }
      //Prints string and then flushes it to file.
      public void print(String c){
          write.println(c);
          write.flush();
      }
}

