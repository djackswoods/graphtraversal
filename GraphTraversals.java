/*
 * No modifications have been made to this file 
 * except for the traversals taking a second string for 
 * my modified saveAs
 */

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;
/**
 *
 * @author Daniel Jackson
 */
public class GraphTraversals {
   //Modified SaveAs to take two strings for open and Save as
    public void depthFirstTraversal(String fileName,String save, int s) 
            throws FileNotFoundException, IOException{
        
   
        Graph G, H;
        G = new Graph();
        G.open(fileName);
        
        H = new Graph( G.numberOfNodes(), G.DIRECTED, G.UNWEIGHTED);
        DFT(G,H,s);
        
        H.print();
        H.saveAs(save);
        
    }
    private void DFT(Graph G, Graph H, int v){
       G.setMark(v, G.MARKED);
       
       for(int w = G.getFirstEdge(v); G.isEdge(v,w); w = G.getNextEdge(v,w)){
           if(G.getMark(w) != G.MARKED ){
               H.setEdge(v,w);
               DFT(G,H,w);
               
           }
       }
    }
    //Modified to take two strings one for Open File and another for Save as
    public void breadthFirstTraversal( String fileName, String save, int s) 
            throws FileNotFoundException, IOException{
        Graph G, H;
        
        ArrayList<Integer> Q;
        int v;
        
        G = new Graph();
        G.open(fileName);
        H = new Graph( G.numberOfNodes(), G.DIRECTED, G.UNWEIGHTED);
        Q = new ArrayList<Integer>();
        G.setMark(s, G.MARKED);
        Q.add(s);
        
        while(! Q.isEmpty()){
            v = Q.remove(Q.size()-1);
            
        for(int w = G.getFirstEdge(v); G.isEdge(v,w); w = G.getNextEdge(v,w) ){
                if(G.getMark(w) != G.MARKED){
                    G.setMark(w, G.MARKED);
                    H.setEdge(v,w);
                    Q.add(w);
                }
            }
            
        }
    H.print();
    H.saveAs(save);
    
    }
    

}

